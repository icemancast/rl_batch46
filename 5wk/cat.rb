require_relative 'animal'

class Cat < Animal

  def initialize(breed, name)
    @breed = breed
    @name = name
  end

  def meow
    "Class cat's meow method: Meow! Meow!"
  end

  def display
    "Class cat's display method: I am of #{@breed} breed and my name is #{@name}."
  end
end
