class Animal
  def display
    'Class animal: display method from animal.'
  end

  def run
    'Class Animal: run method from animal.'
  end
end
