class Bicycle
  attr_reader :gears, :wheels, :seats
  def initialize(gears = 1)
    @wheels = 2
    @seats = 1
    @gears = gears
  end
end

class Tandem < Bicycle
  def initialize(gears)
    super
    @seats = 2
  end
end

t = Tandem.new(2)
puts t.gears # => 2
puts t.wheels # => 2
puts t.seats # => 2
b = Bicycle.new
puts b.gears # => 1
puts b.wheels # => 2
puts b.seats # => 1
