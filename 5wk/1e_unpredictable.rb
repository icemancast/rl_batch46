class String
  def scramble
    self.split('').sort_by {rand}.join(' ')
  end
end

string = "This is just a test for this"
puts string.scramble
