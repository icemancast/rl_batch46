=begin
doctest: I can greet the world
>> hello 
=> "Hello World!"
doctest: I can gree someone by name
>> hello "Isaac"
=> "Hello Isaac!"
doctest: I can ask if someone is there
>> hello("Victor", "?")
=> "Hello Victor?"
=end

def hello name = "World", mark = "!"
  "Hello #{name}#{mark}"   
end
