require 'logger'
$LOG = Logger.new('6wk/log_file.log', 'monthly')
$LOG.level = Logger::ERROR # Only load errors or fatal

def divide(numerator, denominator)
  $LOG.debug("Numerator: #{numerator}, denominator #{denominator}")

  begin
    result = numerator / denominator
  rescue Exception => e
    $LOG.error "Error in division!: #{e}"
    result = nil
  end
  return result
end

divide(10, 0)
#divide(10, 2)
