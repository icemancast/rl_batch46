=begin
doctest: Convert fahrenheit to celsius
>> convert_to_celsius(-40)
=> -40
>> '%.8f' % convert_to_celsius(0)
=> '-17.77777778'
>> convert(212)
=> 100
>> convert(98.6)
=> 37
>> '%.8f' % convert(98)
=> '36.66666667'
=end
def convert_to_celsius fahrenheit
  ( fahrenheit.to_f - 32 ) * 5/9
end

alias :convert :convert_to_celsius

[-40, 0 , 32, 98.6, 100, 212].each do | temp_f |
  puts "%7.2fF = %.2fC" % ([temp_f, convert(temp_f)])
end
