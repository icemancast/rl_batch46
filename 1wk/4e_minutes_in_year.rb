=begin
doctest: Display minutes in year
>> minutes_in_year
=> 525600
=end
minutes_in_hour = 60
hours_in_day = 24
days_in_year = 365
minutes_in_year = minutes_in_hour * hours_in_day * days_in_year
puts minutes_in_year
puts "this is var #{minutes_in_year}"
