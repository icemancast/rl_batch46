=begin
doctest: Should print out age with sentence
>> age_in_seconds = 979000000
=> 979000000
>> seconds_in_year = 60.0 * 60 * 24 * 365
=> 31536000.0
>> age
=> 31.04388635210553
>> report % age
=> 'You are 31.04 years old.'
=end
age_in_seconds = 979000000
seconds_in_year = 60.0 * 60 * 24 * 365
age = age_in_seconds / seconds_in_year
report = "You are %.2f years old."
puts report % age
