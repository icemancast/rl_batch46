# Prints array of numbers given and tells wheter they are odd or even

number_array = [12, 23, 456, 123, 4579]

number_array.each do |n|
  number_status = (n % 2 == 0) ? 'even': 'odd'
  puts "#{n}: #{number_status}"
end
