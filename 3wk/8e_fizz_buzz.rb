# Fizzbuzz program. Print fizz instead of multiples of 3 and buzz for multiples
# of 5 and fizzbuzz for multipes of 3 and 5.

numbers = 1..100

numbers.each do |n|
  if (n % 5 == 0 && n % 3 == 0)
    puts 'FizzBuzz'
  elsif (n % 3 == 0)
    puts 'Fizz'
  elsif (n % 5 == 0)
    puts 'Buzz'
  else
    puts n
  end
end
