# Reverse the order of the words.
def reverse_word_order(string)
  string.split(' ').reverse.join(' ')
end

puts reverse_word_order('test this string')
