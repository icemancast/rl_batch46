# Analyze a text file
File.open('3wk/text.txt', 'r') do |f|
  line_count = f.count
  puts line_count
  character_count_with_spaces = f.size
  puts character_count_with_spaces
  character_count_no_spaces = f.to_s.sub(/ /, '').size
  puts character_count_no_spaces
end
