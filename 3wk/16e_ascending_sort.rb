a = ["Magazine", "Sunday", "Jump"]
=begin
Answers:
1. a.sort
2. a.sort { |s| s }
3. a.sort { |l, r| l <=> r }
4. a.sort { |l, r| l.length <=> r.length }
5. a.sort_by { |s| s }
6. a.sort_by { |s| s.length }

Let's make these into testable lines!

doctest: Setup
>> correct_sort = %w|Jump Sunday Magazine|
=> ["Jump", "Sunday", "Magazine"]
doctest: 1: sort doesn't sort by length
>> a.sort == correct_sort
=> false
doctest: 2: sort with block {|s| s } raises error
>> begin ; a.sort {|s| s} ; rescue => e ; e.class ; end
=> ArgumentError
doctest: 3: sort with block { |l, r| l <=> r } doesn't sort by length
>> a.sort { |l, r| l <=> r } == correct_sort
=> false
doctest: 4: sort with block { |l, r| l.length <=> r.length } This works!
>> a.sort { |l, r| l.length <=> r.length } == correct_sort
=> true
doctest: 5: a.sort_by { |s| s } Alphabetical sort
>> a.sort_by {|s| s} == correct_sort
=> false
doctest: 6: a.sort_by { |s| s.length } This works!
>> a.sort_by { |s| s.length } == correct_sort
=> true
=end
