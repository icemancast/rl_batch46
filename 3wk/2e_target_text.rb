# Get word from file and replace it with new word.
# doctest: Test target replaced by given string
# >> replace_word_in_text('test', 'inserted', 'this is a test. test this')
# => 'this is a inserted. inserted this'
def replace_word_in_text(target, replacement, text)
  myregex = %r[\b#{target}\b]
  text.gsub(myregex, replacement)
end

begin
  file_name = '3wk/2e_text.txt'
  file = File.open(file_name, 'r+')
  text = file.readlines.join
  file.seek(0)
  file.write(replace_word_in_text('word', 'inserted word', text))
end if __FILE__ == $0
