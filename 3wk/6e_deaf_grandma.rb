# Deaf grandma program
year_range = 1930..1950
puts 'Ask grandma a question but be sure to speak loudly (all caps)'
until 'BYE' == user_input = gets.chomp do
  year_random = rand(year_range)
  response = (user_input == user_input.upcase && !user_input.empty?) ? "NO, NOT SINCE #{year_random}" : 'HUH?! SPEAK UP, SONNY!'
  puts response
end
