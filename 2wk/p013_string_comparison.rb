# String#eql?, tests two strings for identical content
# It returns the same result as ==
# String#equal?, tests whether two strings are the same object

s1 = 'Jonathan'
s2 = 'Jonathan'
s3 = s1
if s1 == s2
  puts 'Both strings have identical content'
else
  puts 'Both strings do not have identical content'
end
if s1.eql?(s2)
  puts 'Both strings have identical content'
else
  puts 'Both strings do not have identical content'
end
if s1.equal?(s2)
  puts 'two strings are identical objects'
else
  puts 'two strings are not identical objects'
end
if s1.equal?(s3)
  puts 'two strings are identical objects'
else
  puts 'two strings are not identical objects'
end
