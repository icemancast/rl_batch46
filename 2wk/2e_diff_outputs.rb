=begin
doctest: Should separate string
>> puts string
=> Welcome to the forum.\n
Here you can learn Ruby.\n
Along with other members.
=end

string = "Welcome to the forum.\nHere you can learn Ruby.\nAlong with other members.\n"
string_array = string.split('\n')
string_array.each_index { |x| puts "Line #{x}: " }
