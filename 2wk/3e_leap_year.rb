=begin
doctest: Leap year 1800
>> leap_year?(1800)
=> false
doctest: Leap year 1900
>> leap_year?(1900)
=> false
doctest: Leap year 1996
>> leap_year?(1996)
=> true
doctest: Leap year 2000
>> leap_year?(2000)
=> true
doctest: Leap year 2004
>> leap_year?(2004)
=> true
=end
def leap_year? year
  year % 400 == 0 || year % 100 != 0 && year % 4 == 0
end

def minutes_in_year year
  minutes_in_day = 24 * 60
  days_in_year = leap_year?(year) ? 366 : 365
  minutes_in_day * days_in_year
end

years = [1800, 1900, 1996, 2000, 2004]
years.each do |year|
  puts leap_yar?(year)
  puts minutes_in_year(year)
end
