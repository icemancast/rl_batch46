=begin
doctest: Should separate string
>> linify "Hello\nWorld!"
=> "Line 1: Hello\nLine 2: World!\n"
=end

def linify string
  string.split("\n").map.with_index(1) { |line, i| "Line #{i}: #{line}" }.join("\n")+"\n"
end

begin
  string = "Welcome to the forum.\nHere you can learn Ruby.\nAlong with other members.\n"
  puts linify(string)
end if __FILE__ == $0

