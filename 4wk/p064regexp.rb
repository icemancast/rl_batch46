# Regular expression
string = 'My phone number is (123) 555-1234'
phone_re = /\((\d{3})\)\s+(\d{3})-(\d{4})/
m = phone_re.match(string)
unless m
  puts 'there no match'
  exit
end
print 'The whole string we started with: '
puts m.string
puts m[0]
puts 'The three captures: '
3.times do |index|
 puts "Capture ##{index + 1}: #{m.captures[index]}"
end
puts "here's another way to ge the first capture:"
print "Capture #1: "
puts m[1]
