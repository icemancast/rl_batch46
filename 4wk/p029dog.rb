# define class Dog
class Dog
  def initialize(breed, name)
    @breed = breed
    @name = name
  end

  def bark
    puts 'Ruff! Ruff!'
  end

  def display
    puts "I am of #{@breed} breed and my name is #{@name}"
  end
end

d = Dog.new('Labrador', 'Benzy')

=begin
 To see a list of innate methods, you can call the methods
 method. Remove comment below and execute.
=end
# puts d.methods.sort

# Ever id has an id number
puts "The id of d is #{d.object_id}."

# To know wheter the object knows how to handle the message you want
# to send it, by user the respond_to? method.
if d.respond_to?('talk')
  d.talk
else
  puts 'Sorry, d doesn\'t understand the talk message.'
end

d.bark
d.display

# making d and d1 point to the same object
d1 = d
d1.display

# making d a nil reference, meaning it does not refer to anything
d = nil
d.display

# if I now say
d1 = nil
