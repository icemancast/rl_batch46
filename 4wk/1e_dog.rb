class Dog
  attr_reader :name

  @@dogs_created ||= []
  def initialize(name)
    @name = name
    # We decided to store tricks as names in hash and the value trick
    # as a proc
    @tricks_known = Hash.new # or {}
    @@dogs_created << @name
  end

  def friends
    "My and my friends are: #{@@dogs_created.join(' ')}\nThat means I have #{
      @@dogs_created.size-1} friends!"
  end

  def bark
    'Bark, bark!'
  end

  def eat
    'Feed me!'
  end

  def chase_cat
    'Get that cat'
  end

  def teach_trick(trick, &block)
    # Store the trick as a proc from the block given
    @tricks_known[trick] = block unless @tricks_known[trick]
  end

  # Method Missing -> block or string
  #     doctest: Trick class work?
  #     >> test_dog = Dog.new('Test Dog')
  #     >> test_dog.jump_through_hoops
  #     => "We don't know that jump_through_hoops"
  #     doctest: Teach dog jump_through_hoops
  #     >> name = 'Test Dog'
  #     >> test_dog.teach_trick(:jump_hoops) {"#{test_dog.name} jumps through hoops"}
  #     >> test_dog.jump_hoops
  #     => "Test Dog jumps through hoops"
  def method_missing(method, *args, &blk)
    if @tricks_known[method]
      @tricks_known[method].call
    else
      "We don't know that #{method}"
    end
  end

  # Responsible 'method_missing' authors write 'respond_to?' methods!
  def respond_to? method
    !!@tricks_known[method]
  end

  def self.report
    @@dogs_created
  end
end

if __FILE__ == $0
d = Dog.new('Lassie')
name = 'Lassie'
d.teach_trick(:dance) {"#{name} dances up a storm"}
puts d.dance
puts d.respond_to?(:dance)
puts d.respond_to?(:test)
end
