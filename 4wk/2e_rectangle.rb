=begin
Rectangle Class will give area and perimeter while keeping height and width out of reach
    doctest: Test area for rectangle should equal 120
    >> r = Rectangle.new(10, 12)
    >> r.area
    => 120
    doctest: Test perimeter for rectangle should equal 44
    >> r.perimeter
    => 44
    doctest: Test for negative number
    >> r = Rectangle.new(-10, -12)
    >> r.area
    => nil
    >> r.perimeter
    => nil
=end
class Rectangle
  attr_reader :area, :perimeter

  def initialize (height, width)
    if(height > 0 && width > 0)
      @area = height * width
      @perimeter = 2 * (height + width)
    end
  end
end

r = Rectangle.new(23.45, 34.67)
puts "Area is = #{r.area}"
puts "Perimeter is = #{r.perimeter}"
