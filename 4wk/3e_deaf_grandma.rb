# Deaf grandma program
class DeafGrandma
  def initialize
    @name = 'Granny'
  end

  def response(user_input)
    shouted?(user_input) ? deafness_level('heard') : deafness_level('not_heard')
  end

  def shouted?(words)
    (words == words.upcase && !words.empty?)
  end

  def deafness_level(level)
    case level
    when 'heard'
      return "NOT SINCE #{rand(years_remembered)}"
    when 'not_heard'
      return 'HUH?! SPEAK UP, SONNY!'
    end
  end

  def years_remembered(range = 1930..1950)
    range
  end

end

bye_count = 0
granny = DeafGrandma.new
puts 'Ask grandma a question but be sure to speak loudly (all caps)'
until bye_count == 3
  user_input = gets.chomp
  response = granny.response(user_input)
  'BYE' == user_input and bye_count += 1 or bye_count = 0
  puts response
end
