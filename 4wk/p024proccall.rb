prc = lambda {puts 'Hello'}
# method call invokes the block
prc.call

# another example
toast = lambda do
  'cheers'
end
puts toast.call
